About
-----
This is a simple plugin for the Ruby IRC bot rbot, which can be found
at http://linuxbrit.co.uk/rbot .

The plugin allows rbot instances to fetch information about bugs
present on a given bugzilla installation by fetching the XML data of
the requested bug, and then shows a summary line on the channel or the
query where it was requested.

A default set of configured Bugzilla installations can be found in the
Berkeley DB dump file in this tarball, this includes Bugzilla
installations for: Gentoo, xine, Linux kernel, KDE, GNOME, SourceMage
Linux, Mozilla, FreeDesktop, Netbeans, SpamAssassin, Eclipse, Apache,
GCC, atheme (Audacious and other projects), Novell and RedHat.

Instructions
------------
The default Bugzilla installations data is included as an ASCII text dump
suitable for loading into the Berkeley DB.

If you want to load this into your rbot instance, just create the registry
before starting the bot with the command:

db_load ${RBOTHOME}/registry/bugzillaplugin.db < registry.dump

Note: on some distributions, like Gentoo, the db_load command has to be
replaced by the proper version of the Berkley DB command to use, like
db4.6_load.

Requirements
------------
Optionally, the plugin can use HTML Entities for Ruby to decode the
HTML entities referenced in the bugs' descriptions, which is available
at http://htmlentities.rubyforge.org/ .

Licensing
---------
The plugin is released under the Affero General Public License 3 (or
at your option, any later version). For this reason, the "zilla
source" download will report the URL where to download the source.

If you make any change to the source code, you should then make them
available, and change the plugin_sources function accordingly.
